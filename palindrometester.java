import java.util.Scanner;


public class palindrometester {
//declared askUser variable 
	public static char askUser;
	
	//wrote a boolean method that checks if the string entered by the user is a palindrome, returns true or false
	public static boolean checkPalindrome(String input2){
		if( input2.length() == 1){
			return true;
		}
		
		
		
		if(input2.charAt(0) == input2.charAt(input2.length()-1)){
			
			//i keep calling method checkPalindrome to check every character in the string to compre it with 
			//the original string
			//
			return checkPalindrome(input2.substring(1, input2.length()-1));
			
		}
		else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//used do while loop which allow the program to prompt the user if he would like to check another palindrome
		
		do
		{
		System.out.println("Enter a potential palindrome");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		
		String lowerCase = input.nextLine();
		
		//make letters entered in string lowercase
		
		lowerCase = lowerCase.toLowerCase();
		// int lengthofString = lowerCase.length();
		
		//checks if the string contains numbers , if it does it notfies user to type only letters 
		if(lowerCase.matches("[0-9]+"))
		{
			System.out.println("Please enter only letters, Spaces are not allowed");
			//System.exit(0);
		}
		
		//calls method checkPalindrome to check if the string is a palindrome
		else {
		if(checkPalindrome(lowerCase)){
			System.out.println("COngrats! Its a palindrome");
		}
		else 
		{
			System.out.println("Its not a palindrome :(");
			//System.exit(0);
		}
		
		}System.out.println("Would you like to continue (Y/N)");
		 askUser = input.nextLine().charAt(0);
		//input.close();
		
		}
		//program will continue to run until the user selects N or any letter besides Y 
		while(askUser=='Y' || askUser=='y');
		
	}

}
